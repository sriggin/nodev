'use strict';
/**
 * From cradle readme @ https://github.com/flatiron/cradle
 */
var cradle = require('cradle'),
    url = process.env.CLOUDANT_URL || process.env.COUCHDB_URL || 'http://localhost:5984',
    conn = new (cradle.Connection)(url, {
        cache: true,
        raw: false,
        forceSave: true
    }),
    db = conn.database('twittle');
//create the design documents and db if it doesn't exist
db.exists(function initializeUsers(err, exists) {
    if (err) {
        console.log('error', err);
    } else if (exists) {
        console.log('Twittle DB exists');
    } else {
        console.log('Creating Twittle DB');
        db.create();
        //Users design document
        db.save('_design/users', {
            views: {
                byUsername: {
                    map: function (doc) {
                        if (doc.resource === 'User') {
                            emit(doc.username, doc);
                        }
                    }
                }
            }
        });
        //Messages design document
        db.save('_design/messages', {
            views: {
                byUser: {
                    map: function (doc) {
                        if (doc.resource === 'Message') {
                            emit(doc.userId, null);
                        }
                    }
                }
            }
        });

        //bootstrap data
        // create initial test user per spec
        //circular dependency -- but it's async, so it works
        require('./users').createUser('test', 'test', function wrapUp(err) {
            if (err) {
                console.log('Failed to create test user', err);
            }
        });
    }
});

// Exports from couchdb wrapper
module.exports = {
    db: db,
    /**
     * Function to extract a document from a result row, useful with _.map
     * @param {Object} row Result row
     * @returns {*} Value of row.doc
     */
    getDoc: function getDocFromRow(row) {
        return row.doc;
    },
    /**
     * Function to extract a value from a view result row, useful with _.map
     * @param {Object} row Result row
     * @returns {*} value of row.value
     */
    getValue: function getValueFromRow(row) {
        return row.value;
    },
    /**
     * Function to extract the row ID from a result row, useful with _.map
     * @param {Object} row Result row
     * @returns {document._id} value of row._id
     */
    getId: function getIdFromRow(row) {
        return row._id;
    }
};
