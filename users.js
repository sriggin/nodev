/**
 * @callback rowCallback
 * @param {*} err Error object, if truthy
 * @param {Object} row Result row
 */
/**
 * @callback rowSetCallback
 * @param {*} err Error object, if truthy
 * @param {Array.<Object>} rowSet Result rows
 */
/**
 * @callback userCallback
 * @param {*} err Error object, if truthy
 * @param {User} user Result user
 */
/**
 * @callback resultCallback
 * @param {*} err Error object, if truthy
 * @param {object} result CouchDB result
 */
/**
 * @callback userArrayCallback
 * @param {*} err Error object, if truthy
 * @param {Array.<User>} users Array of users
 */
/**
 * @callback followingCallback
 * @param {*} err Error object, if truthy
 * @param {Object} users Object containing mappings of User ID to User name
 */


'use strict';
var couchdb = require('./couchdb'),
    crypto = require('./crypto'),
    db = couchdb.db,
    key = couchdb.key,
    ns = crypto.ns,
    shuffle = crypto.shuffle,
    _ = require('lodash');
/**
 * Constructs a new User instance
 * @param username Name of the user to construct
 * @constructor
 */
function User(username) {
    this.username = username;
    this.resource = 'User';
    this.following = [];
}
/**
 * Constructs a User instance from a CouchDB document
 * @param doc CouchDB User document
 * @returns {User}
 */
User.fromDoc = function fromDoc(doc) {
    if (doc.resource !== 'User') {
        throw 'Wrong document type';
    }
    var u = new User(doc.username);
    u.id = doc._id;
    u.following = doc.following;
    return u;
};

/**
 * Returns an object with the hashed/salted form of the given password
 * @param {String} password Plain-text password to set
 * @param {Object} [user={}] Object to set the password in.
 * @returns {Object} user object with hashed password and salt set
 */
function storePassword(password, user) {
    var result = user || {},
        rnd = Math.floor((Math.random() * 10000000)),
        username = result.username || '',
        sha512 = crypto.sha512,
        salt = result.salt = sha512(shuffle(username + ns() + rnd + password));
    result.hashed = sha512(salt + password);
    return result;
}
/**
 * Calls success with a row matching the given key at most once.
 * @param {String} key Value to match for row's key
 * @param {rowCallback} next Success callback called if row is found
 * @returns {rowSetCallback} Callback for handling key search, to be passed to cradle
 */
function findKey(key, next) {
    return function handleData(err, data) {
        if (err) { //handle error if provided
            next(err, false);
        } else {
            for (var i in data) { //loop through results
                var row = data[i];
                if (key === row.key) { //try to find matching key
                    next(null, row.value); //only call success with match
                    return;
                }
            }
            next('Row not found', false);
        }
    };
}

/**
 * Attempts to find the user object with the given username
 *
 * @param {String} username Name of the user to find
 * @param {userCallback} next Callback to invoke on completion
 */
function findUserByName(username, next) {
    db.view('users/byUsername', {key: username}, findKey(username, next));
}

/**
 * Builds a function to verify that a user's password matches
 *
 * @param {String} password
 * @param {userCallback} next
 */
function matchPassword(password, next) {
    return function testPassword(err, user) {
        if (err) {
            next(err, false);
        } else {
            var hashed = crypto.sha512(user.salt + password);
            if (hashed === user.hashed) {
                next(null, user);
            } else {
                next('Password does not match', false);
            }
        }
    };
}
/**
 * Matches the provided username+password to authenticate the user
 *
 * @param {String} username
 * @param {String} password
 * @param {userCallback} next
 */
function authenticate(username, password, next) {
    findUserByName(username, matchPassword(password, next));
}
/**
 * Builds a function to update the password for a provided user
 *
 * @param {String} newPassword New password for the user
 * @param {resultCallback} next
 */
function setPassword(newPassword, next) {
    return function updatePassword(err, user) {
        if (err) {
            next(err, false);
        } else {
            db.merge(user._id, storePassword(newPassword), next);
        }
    };
}
/**
 * Changes the password of the user matching the given username/password
 *
 * @param {String} userId ID of the user to update
 * @param {String} password Current password for the user
 * @param {String} newPassword New password to set for the user
 * @param {userCallback} next
 */
function changePassword(userId, password, newPassword, next) {
    db.get(userId, matchPassword(password, setPassword(newPassword, next)));
}
/**
 * Retrieves a list of users in the system
 *
 * @param {userArrayCallback} next User Array Callback
 */
function getUsers(next) {
    //return an array of users in the system
    db.view('users/byUsername', function formatUsers(err, users) {
        if (err) {
            next(err, false);
        } else {
            var results = [];
            for (var name in users) {
                results.push(User.fromDoc(users[name].value));
            }
            next(null, results);
        }
    });
}
/**
 * Creates a new user document in the users database
 *
 * @param {String} username Name of the user
 * @param {String} password Plain-text password for the user
 * @param {resultCallback} next Result callback
 */
function createUser(username, password, next) {
    db.save(storePassword(password, new User(username)), next);
}

/**
 * Gets the User with the given id
 * @param {String} userId ID of the user to find
 * @param {userCallback} next User callback
 */
function getById(userId, next) {
    db.get(userId, next);
}
/**
 * Sets the following list for the user with the given ID
 * @param {String} userId ID of the user to update
 * @param {Array.<String>} following IDs of users followed
 * @param {resultCallback} next Results Callback
 */
function updateSubs(userId, following, next) {
    db.merge(userId, {
        following: following
    }, next);
}
/**
 * Passes normalized array of users followed by the given user  to the
 *  next callback
 * @param {String} userId ID of the user to get list of followers for
 * @param {followingCallback} next Following Users Callback
 */
function withSubs(userId, next) {
    getById(userId, function handleSubs(err, user) {
        if (err) {
            next(err, false);
        } else {
            next(null, user.following || []);
        }
    });
}
/**
 * Add target user to list of followed for the given user
 * @param {String} userId ID of the user to update the list of following
 * @param {String} targetUserId ID of the user to follow
 * @param {resultCallback} next Result Callback
 */
function subscribe(userId, targetUserId, next) {
    withSubs(userId, function addSub(err, following) {
        if (err) {
            next(err, false);
        } else if (0 > following.indexOf(targetUserId)) {
            following.push(targetUserId);
            updateSubs(userId, following, next);
        } else {
            next('Already following user', false);
        }
    });
}
/**
 * Remove target user from list that the given user is following
 * @param {String} userId ID of the user to update the following list for
 * @param {String} targetUserId ID of the user to stop following
 * @param {resultCallback} next Result Callback
 */
function unsubscribe(userId, targetUserId, next) {
    withSubs(userId, function removeSub(err, following) {
        if (err) {
            next(err, false);
        } else {
            var index = following.indexOf(targetUserId);
            if (index > -1) {
                following.splice(index, 1);
                updateSubs(userId, following, next);
            } else {
                next('Not following user', false);
            }
        }
    });
}

/**
 * Gets list of users that the specified user is following
 * @param {User} user Who to get the list of followed users for
 * @param {followingCallback} next
 */
function getFollowing(user, next) {
    db.all({
        keys: user.following || [],
        'include_docs': true
    }, function (err, users) {
        if (err) {
            next(err);
            return;
        }
        //wooh, map-reduce!
        next(null, _.reduce(_.map(users, couchdb.getDoc), function (obj, row) {
            obj[row._id] = row.username;
            return obj;
        }, {}));
    });
}

module.exports = {
    authenticate: authenticate,
    getUsers: getUsers,
    changePassword: changePassword,
    createUser: createUser,
    getById: getById,
    subscribe: subscribe,
    unsubscribe: unsubscribe,
    getFollowing: getFollowing
};
