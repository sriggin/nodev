# twittle - Node.JS Microblogging Assignment
I'm very creative with names.

## Requirements

_twittle_ requires the following bits of software to work:

* Node.js
* CouchDB
* _(Optional)_ Foreman

These packages are available in _apt-get_ for _Ubuntu_, and all of these have _Windows_ installers.

## Setup

1.  Extract the archive, or clone the repository, to a working directory:

    ```
    git clone <url> twittle
    
    cd twittle
    ```

2.  Install _node_ dependencies using `npm`:

    ```
    npm install
    ```

3.  Configure the system according to the directions under the next section.

## Configuration

_twittle_ is configured using environment variables, and uses default values that would make sense for a development/test system

* HTTP port (default=`5000`):
    * `PORT` - Port for the Node.js HTTP server to listen on
* CouchDB Connection URL (default=`http://localhost:5984`)
    * `COUCHDB_URL` - The fully qualified/configured URL to connect to CouchDB. If authentication is required, credentials should be included in this url. This has priority over the next variable.
      Example: `http://user:pass@someserver.com:5984`
    * `CLOUDANT_URL` - If _Cloudant Data as a service_ is used, set this URL to configure it. This is primarily meant to support _Heroku_ deployment

## Usage

Once installed, the application can be started two ways:

* Directly with _node_:

    ```
    node web.js
    ```

* Using _foreman_, to simulate execution in _Heroku_:

    ```
    foreman start
    ```

The project can be viewed at `http://<hostname>:<port>/`. If running locally, without changing the port, [the project can be viewed here](http://localhost:5000/).

## Assignment Specifications

Per the assignment instructions, the requirements can be reviewed as follows:

> 1) List the users in the system.

A list of users in the system can be viewed on the **Friends** page.

> 2) Allow subscribing to and unsubscribing from the users in that list.

Users listed on the **Friends** page can be subscribed/unsubscribed to based on their current subscription status.

> 3) Show the last 20 messages from the users you're subscribed to.

The last 20 messages from the other users that a user is subscribed to, as well as their own, are shown on the **My Feed** page.

> 4) Write messages of your own.

At the top of the **My Feed** page, there is a form for entering and posting messages <= 140 characters in length. A counter is provided for convenience.

Images (`<img src="...">`) and Links (`<a href="...">`) may be provided in comments, to facilitate sharing of cat pictures. It's not a social app without cat pictures. Only the described attributes are supported for these elements.

The current user's messages are presented right-aligned to visually distinguish them from messages of subscribed users.

> 5) Re-post someone else's message such that it gives the original author attribution in your message (essentially a retweet)

Any message on the **My Feed** page not posted by the current user will have a **Repeat** link at the top-right of the message.

Repeated messages are marked at the top-left of the message with who repeated them, while maintaining the display style and information from the original message, including alignment.

> 6) Support changing your password.

The current user can change their password by opening the **Settings > Change Password** link from the top. For _safety_ the user must enter their current password, to try to dissuade office hijinks.

## Source

[Available on BitBucket](https://bitbucket.org/sriggin/nodev)

## Additional Info

An **About** page is included which covers various technologies used throughout the stack (including the human portion) as well as some considerations on certain aspects of the technical implementation.

Despite the omission from the assignment, a REST call to create a user has been included to bootstrap the system for testing.

```
Method: PUT
URL: /api/users
Accepts: application/json
Body:
  {
      username [String],
      password [String]
  }
```

And above all, _please be mean_! The more feedback that I can solicit from this, the more I can learn!
