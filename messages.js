/**
 * @callback messageCallback
 * @param {*} err Error object
 * @param {Message} message Message object
 */
/**
 * @callback messageArrayCallback
 * @param {*} err Error object
 * @param {Array.<Message>} messages Array of messages
 */

'use strict';
var couchdb = require('./couchdb'),
    db = couchdb.db,
    users = require('./users'),
    Heap = require('heap'),
    _ = require('lodash');
// initialize messages database
/**
 * Creates a new Message
 * @constructor
 */
function Message(user, text, time) {
    //this person should exist
    this.userId = user._id;
    this.username = user.username;
    this.text = text;
    this.time = time;
    this.resource = 'Message';
}
/**
 * Creates a retweet message from an existing message
 * @param {User} user User who sent the tweet
 * @param {Message} message original message
 * @param {Date} time Time the retweet was created
 */
Message.retweet = function retweet(user, message, time) {
    var rt = new Message(user, message.text, time);
    rt.orig = message;
    return rt;
};
/**
 * Posts a new message
 * @param {User} user User who sent the tweet
 * @param {String} text Message, <= 140 chars
 * @param {resultCallback} next Callback
 */
function postMessage(user, text, next) {
    if (!text) {
        next('Message body is required');
    } else if (text.length > 140) {
        next('Message body may not be more than 140 chars');
    } else {
        db.save(new Message(user, text, Date.now()), next);
    }
}
/**
 * Finds the message with the given ID and passes it (or error) to next
 * @param {String} messageId ID of the message to find
 * @param {messageCallback} next Next callback
 */
function getMessage(messageId, next) {
    db.get(messageId, next);
}
/**
 * Re-tweets the message with the given ID from the given user
 * @param {String} userId ID of the user sending the retweet
 * @param {String} messageId ID of the message to retweet
 * @param {resultCallback} next Next callback
 */
function retweetMessage(userId, messageId, next) {
    getMessage(messageId, function retweetIt(err, message) {
        if (err) {
            next(err, false);
        } else {
            var rt = Message.retweet(userId, message, Date.now());
            db.save(rt, next);
        }
    });
}
/**
 * Applies the given extractor to the given coll array, pushing the results onto a heap, then extracting count elements
 * @param {Array} coll Collection to sort
 * @param {number} count Number of results to return
 * @param {Function} [extract=identity] Function to apply to each element extracted from coll
 * @returns {Array} Sorted array containing (count) elements
 */
function sortAndSlice(coll, count, extract) {
    var getter = extract || _.identity,
        results = new Heap(function (a, b) {
            return b.localeCompare(a);
        }),
        keys = [],

        i = 0;
    //apply the extractor to every entry and push the result onto the heap
    coll.forEach(function addToHeap(entry) {
        results.push(getter(entry));
    });
    //transfer IDs from heap to key list
    while (i++ < count) {
        var id = results.pop();
        if (!id) {
            break;
        }
        keys.push(id);
    }
    return keys;
}
/**
 * Gets the most recent 20 messages from the specified user and the users they're following
 * @param {User} user User to get the feed for
 * @param {messageArrayCallback} next Next callback to invoke with returned feed
 */
function getUserFeed(user, next) {
    //get all messages from users the given user is following, including the given user, with no duplicates
    db.view('messages/byUser', {
        keys: _.chain(user.following || []).union([user._id]).unique().value()
    }, function getMessageIds(err, messages) {
        if (err) {
            next(err);
            return;
        }
        //pluck out and sort message IDs
        var keys = sortAndSlice(messages, 20, function (msg) {
            return msg.id;
        });
        //get the list of sorted messages using the list of sorted IDs
        db.all({keys: keys, 'include_docs': true}, function formatMessages(err, messages) {
            next(err, err || _.map(messages, couchdb.getDoc));
        });
    });
}

//get messages from OPs who user is subscribed to

module.exports = {
    postMessage: postMessage,
    retweetMessage: retweetMessage,
    getUserFeed: getUserFeed
};
