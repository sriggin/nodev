/* global $ */
'use strict';

/** Updated first whenever an API call is made */
var currentUser = {
        name: false,
        id: false
    },
    /** Handles registration and delegation of routing based on hash changes in the url */
    router = (function Router() {
        /** private vars */
        var routes = [],
            defaultRoute = false;

        /**
         * Registers a route with a path and corresponding handler function
         * @param {String} path URL pattern after the hash (#) to match. Exclude the #
         * @param {Function} handler Callback function to invoke when the route is opened
         */
        function addRoute(path, handler) {
            routes.push({
                pattern: new RegExp('^#' + path + '$'),
                handler: handler
            });
        }

        /**
         * Executes a route handler corresponding to a matching path, or the default (if registered)
         */
        function navigate() {
            var path = window.location.hash || '',
                isRouted = false;
            //test each route in the order they were registered
            $.each(routes, function testRoute(i, route) {
                var match = route.pattern.exec(path);
                //if the path matches, invoke the handler and exit the each() fn
                if (match) {
                    match.shift();
                    route.handler.apply(route, match);
                    isRouted = true;
                    return false;
                }
            });
            if (!isRouted && defaultRoute) {
                defaultRoute();
            }
        }

        //capabilities
        return {
            addRoute: addRoute,
            navigate: navigate,
            routeDefault: function routeDefault(handler) {
                defaultRoute = handler;
            }
        };
    }()),
    /** short alias for registering a router/handler */
    route = router.addRoute;

/**
 * Loads the view html file relative to the views dir, sets the view in the main div, and invokes the next callback
 * @param {String} file View filename
 */
function loadView(file) {
    return $.get('views/' + file).done(function onGet(data) {
        $('div#main').html(data);
    });
}

/**
 * Displays a closeable alert box to provide feedback to the user.
 * @param {String} message Message text to display
 * @param {jQuery|String} [$target='#main'] Selector to prepend the alert element to
 * @param {String} [alertClass='alert-danger'] CSS class to use for the alert
 */
function showAlert(message, $target, alertClass) {
    var $elem = $('<div class="alert alert-dismissable">' +
        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
        '</div>').append(message).addClass(alertClass || 'alert-danger');
    $($target || '#main').prepend($elem);
}

/**
 * Extracts the user name and ID from the response headers of an API call and updates the current user values
 * @param {jqXHR|XmlHttpRequest} xhr AJAX object containg request/response details
 */
function getUserHeaders(xhr) {
    currentUser.id = xhr.getResponseHeader('X-User-ID');
    currentUser.name = xhr.getResponseHeader('X-User-Name');
    $('.display-username').html(currentUser.name);
}

/**
 * Handles header updates prior to calling success handler
 * @param {Function} [next] Normal success handler to call after updating headers
 * @returns {Function} success callback function
 */
function successHandler(next) {
    return function handleSuccess(data, textStatus, xhr) {
        getUserHeaders(xhr);
        if (next) {
            next(data, textStatus, xhr);
        }
    };
}

//register default route to homepage
router.routeDefault(function home() {
    loadView('home.html');
});

//register handler for user's feed
route('/feed', function feed() {
    /**
     * Perform a POST requset to either create or repeat a message
     * @param url Message API URL to invoke
     * @param data Request body data
     * @returns {promise} jQuery promise for sending the message
     */
    function handleMessage(url, data) {
        return $.ajax(url, {
            type: 'POST',
            contentType: 'application/json',
            data: data && JSON.stringify(data),
            success: successHandler()
        }).success(feed).fail(function () {
            showAlert('Failed to send message', '#post-message');
        });
    }

    /**
     * Constructs an event handler function to repeat a message with the given id
     * @param {String} msgId ID of the message to repeat.
     * @returns {Function} Event handler function
     */
    function onRetweetClick(msgId) {
        return function (event) {
            event.preventDefault();
            handleMessage('/api/messages/' + msgId, '');
        };
    }

    //loads the user's feed page and sets up elements
    loadView('profile.html').done(function onLoad() {
        var $msg = $('#message'),
            $count = $('#message-char-count');

        //setup message box
        $('#post-message').submit(function submitMessage(event) {
            event.preventDefault();
            if ($msg.val().length > 140) {
                showAlert('Message cannot be more than 140 characters', '#post-message');
            } else {
                handleMessage('api/messages', {message: $msg.val()});
            }
        }).on('change keyup paste', function () {
            //handle any changes to the text box by updating the character countf
            $count.html($msg.val().length);
        });
    }).done(function loadFeed() {
        var $feed = $('.feed'),
            $tweetBase = $('.tweet').hide();
        //load message for feed
        $.get('/api/messages', successHandler()).done(function onFeedLoad(data) {
            //response is json
            $.each(data, function (idx, message) {
                var $tweet = $tweetBase.clone().hide(),
                    $retweet = $tweet.find('.forward-tweet'),
                    $repeatHeader = $tweet.find('.repeat'),
                    $sender = $tweet.find('.tweet-sender');
                //handle retweets slightly differently
                if (message.orig) {
                    $repeatHeader.find('.repeat-sender').html(message.username);
                    message = message.orig;
                } else {
                    $repeatHeader.hide();
                }
                //populate elements
                $tweet.find('.tweet-message').html(message.text || '-message not available-');
                $tweet.find('.tweet-time').html(new Date(message.time));
                if (currentUser.id === message.userId) {
                    $sender.html('me');
                    $retweet.remove();
                    $tweet.find('.tweet-quote').addClass('blockquote-reverse');
                } else {
                    //don't let me retweet myself. that's silly
                    $sender.html(message.username || 'Unknown');
                    $retweet.click(onRetweetClick(message._id));
                }
                //add tweet to feed
                $feed.append($tweet);
            });
            $tweetBase.remove();
            $('.tweet').show();
        });
    });
});
//create route handler and page loader for friend list
route('/friends', function friends() {
    /**
     * Handles the user clicking 'follow' or 'unfollow' for a user
     * @param {String} which - ('follow' or 'unfollow') Whether to follow or unfollow
     * @param {String} userId ID of the user to perform the action on
     * @returns {Function} Event handler
     */
    function clicker(which, userId) {
        return function (e) {
            e.preventDefault();
            $.ajax('api/users/' + userId + '/follow', {
                type: {'follow': 'PUT', 'unfollow': 'DELETE'}[which],
                sucess: successHandler()
            }).done(friends).fail(function () {
                showAlert('Failed to ' + which + ' user');
            });
        };
    }

    //load friends
    loadView('friends.html').success(function onLoad() {
        var $friendList = $('.friend-list'),
            $friendBase = $friendList.find('.friend').hide();

        $.get('api/users/following', successHandler()).done(function setFollowing(following) {
            $.get('/api/users').success(function loadFriends(data) {
                $.each(data, function addUser(idx, user) {
                    //ignore showing the current user themself
                    if (user.id !== currentUser.id) {
                        var $friend = $friendBase.clone().attr('data-user-id', user.id).hide(),
                            $friendName = $friend.find('.friend-link'),
                            $follow = $friend.find('a.follow-add').click(clicker('follow', user.id)),
                            $unfollow = $friend.find('a.follow-remove').click(clicker('unfollow', user.id));
                        $friendName.html(user.username);
                        //setup [un]follow links
                        if (user.id in following) {
                            $follow.hide();
                            $unfollow.show();
                        } else {
                            $unfollow.hide();
                            $follow.show();
                        }
                        $friendList.append($friend);
                    }
                });
                $friendBase.remove();
                $friendList.find('.friend').show();
            });
        });
    });
});

//show password change page
route('/passwordchange', function loadPasswordChange() {
    return loadView('passwordchange.html').success(function onLoad() {
        $('#passwordchange').submit(function submitPassword(event) {
            event.preventDefault();
            var $oldPass = $('#oldPassword'),
                $newPass = $('#newPassword'),
                $confirm = $('#confirmPassword');
            if ($newPass.val() !== $confirm.val()) {
                $newPass.parent().addClass('has-error');
                $confirm.parent().addClass('has-error');
                showAlert('New password does not match confirmed', '#passwordchange');
            } else {
                $newPass.parent().removeClass('has-error');
                $confirm.parent().removeClass('has-error');
                $('.alert').remove();
                $.ajax('/api/users/password', {
                    data: JSON.stringify({
                        oldPassword: $oldPass.val(),
                        newPassword: $newPass.val()
                    }),
                    contentType: 'application/json',
                    type: 'POST',
                    success: successHandler()
                }).done(function () {
                    loadPasswordChange().done(function () {
                        showAlert('Password successfully updated', '#passwordchange', 'alert-info');
                    });
                }).fail(function () {
                    showAlert('Failed to update password, verify that your old password is correct', '#passwordchange');
                });
            }
        });
    });
})
;
//show about page
route('/about', function () {
    loadView('about.html');
});

//handle document loaded
$(document).ready(function initDocument() {
    //handle changes to the hash part of the URL
    $(window).on('hashchange', function updateLocation() {
        router.navigate();
    });
    router.navigate();
}).ajaxSuccess(function (event, xhr, settings) {
    //API calls return userID and username, we'll capture these
    if (/^\/api/.test(settings.url)) {
        getUserHeaders(xhr);
    }
});