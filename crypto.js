'use strict';
var crypto = require('crypto');
/**
 * Simple function builder for constructing a few hashers
 * @param {String} algo Name of the hash algorithm to use
 * @returns {Function} A simple function that converts an input to the appropriate hex/hash form
 */
function hashIt(algo) {
    return function algoHash(data) {
        return crypto.createHash(algo).update(data).digest('hex');
    };
}
//Most of these calls are meant to replicate functionality from the provided PHP framework
module.exports = {
    sha1: hashIt('sha1'),
    sha512: hashIt('sha512'),
    ns: function nanos() {
        //return NS component of current second from hi-res timer
        return process.hrtime()[1];
    },
    shuffle: function shuffleStr(str) {
        //taken from answer here: http://stackoverflow.com/questions/3943772/how-do-i-shuffle-the-characters-in-a-string-in-javascript
        return str.split('').sort(function () {
            return Math.random() / 2; //not nearly random enough
        }).join('');
    }
};
