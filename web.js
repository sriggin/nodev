'use strict';
/**
 * Taken from Express getting started guide:
 * http://expressjs.com/guide.html
 */
var express = require('express'),
    app = express(),
    users = require('./users'),
    messages = require('./messages'),
    sanitizer = require('sanitize-html');

//middleware to log errors
function logErrors(err, req, res, next) {
    console.log(err.stack);
    next(err);
}
//middleware to send a bland response for errors to AJAX requests
function clientErrorHandler(err, req, res, next) {
    if (req.xhr) {
        res.send(500, {
            error: 'Internal Server Error'
        });
    } else {
        next(err);
    }
}
//Generic error handler for no matched routes
function errorHandler(req, res) {
    res.send(404, 'Nothing found at ' + req.path);
}
/*
 * middleware configurations
 */
//handle authentication -- this should be attached to a cache
app.use(express.basicAuth(users.authenticate));
app.use(express.static('public'));
app.use(express.json({
    strict: true,
    limit: 2048
}));
app.use(express.methodOverride());
app.use(app.router);
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

/**
 * Utility function builder to construct general send-response callback
 * @param {Response} res Response object
 * @returns {callback} Callback to write output to response
 */
function andSend(res) {
    return function writeOut(err, data) {
        if (err) {
            console.dir(err, err.stack);
            res.send(err.status || 500, err.message || err.body || err);
        } else {
            res.send(data);
        }
    };
}
/**
 * All API calls (anything not handled by the 'static' middleware) will get user ID and name headers set
 */
app.get('/*', function (req, res, next) {
    res.set('X-User-ID', req.user._id);
    res.set('X-User-Name', req.user.username);
    next();
});
/**
 * API calls
 */
//get all users in the system
app.get('/api/users', function listUsers(req, res) {
    users.getUsers(andSend(res));
});
//change the current user's password
app.post('/api/users/password', function changePassword(req, res) {
    var oldPass = req.body.oldPassword,
        newPass = req.body.newPassword;
    users.changePassword(req.user._id, oldPass, newPass, andSend(res));
});
//create a new user ... not a part of the spec
app.put('/api/users', function createUser(req, res, next) {
    var username = sanitizer(req.body.username, {}),
        password = req.body.password;
    if (!username) {
        next({
            status: 400,
            reason: 'Username not provided'
        });
    } else {
        users.createUser(username, password, andSend(res));
    }
});
//get the users that the current user is following
app.get('/api/users/following', function (req, res) {
    users.getFollowing(req.user, andSend(res));
});
//follow the given user
app.put('/api/users/:id/follow', function follow(req, res) {
    users.subscribe(req.user._id, req.params.id, andSend(res));
});
//stop following the given user
app.delete('/api/users/:id/follow', function unfollow(req, res) {
    users.unsubscribe(req.user._id, req.params.id, andSend(res));
});
//post a new message
app.post('/api/messages', function postMessage(req, res) {
    var msg = sanitizer(req.body.message, {
        allowedTags: ['a', 'img'],
        allowedAttributes: {
            a: ['href'],
            img: ['src']
        },
        selfClosing: ['img']
    });
    messages.postMessage(req.user, msg, andSend(res));
});
//repeat an existing message
app.post('/api/messages/:id', function retweet(req, res) {
    messages.retweetMessage(req.user, req.params.id, andSend(res));
});
//get the feed messages for the current user
app.get('/api/messages', function feed(req, res) {
    messages.getUserFeed(req.user, andSend(res));
});

//configure server to listen
var server = app.listen(process.env.PORT || 5000, function () {
    console.log('Listening on port %d', server.address().port);
});